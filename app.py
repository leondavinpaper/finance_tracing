from flask import Flask, request, jsonify, render_template, redirect, url_for, send_file
import pandas as pd
import numpy as np
import warnings
import json
import template as tp
import os
from google.cloud import storage
import datetime
import re
import finance_tracing as finance

from google.cloud import storage

app = Flask(__name__)

warnings.filterwarnings("ignore")

# PROJECTID_PPRPROD = 'paper-prod'
# PROJECTID_PPRPROD = 'skilled-compass-218404'
# dataset           = 'datascience_public'
# table_name        = 'bd_external_data_araas'
# table_name2       = 'bd_external_data_araas_balance'
# table_name3       = 'bd_external_data_ap_balance'
# table_name4       = 'bd_external_credit_limit'

credential_location = "creds/skilled-compass-218404-191e947caac7.json"
BUCKET_NAME    = "kubeflow-skilled-compass-218404"

app=Flask(__name__)

@app.route('/finance_tracing', methods=['GET', 'POST'])
def finance_tracing() :
    if request.method == 'POST':
        if (request.form['generate'] == 'Search') | (request.form['generate'] == 'Download') :
            print(os.path)
            generate = request.form['generate']
            external_ids = request.form['external_id']
            
            external_ids = re.sub(',', ' ', external_ids)
            external_ids = external_ids.split()
            external_ids = [re.sub('\n', '', x) for x in external_ids]
            
            trx3 = finance.main(external_ids)
            
            if generate == 'Search' :
                return trx3.to_html()
            elif generate == 'Download' :
                print(os.path)
                test = pd.read_excel(r'finance_tracing.xlsx', engine = 'openpyxl')
                print(test.head(5))
                
#                 temp = pd.DataFrame({'a' : ['a']})
                
                trx3.to_excel('finance_tracing.xlsx', index = False)
                
#                 test = pd.read_excel(r'fraud_tracing.xlsx', engine = 'openpyxl')
#                 print(test.head(5))
                
                return send_file('finance_tracing.xlsx')
        else :
            return redirect(url_for("menu"))
        
    return render_template('finance_tracing.html')
            

if __name__ == "__main__":
    # from waitress import serve
    # serve(app, host='0.0.0.0', port=5001)
    app.run(host = '0.0.0.0', port = 5000, threaded = True)