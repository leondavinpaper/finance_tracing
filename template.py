import pandas as pd
import numpy as np
import re
import pandas as pd
from google.cloud import bigquery
import os
import pymysql
import pandas as pd
import pygsheets
import pandas_gbq

#GCP to DF from SQL

def gcp2df(sql, client) :
    query = client.query(sql)
    results = query.result()
    return results.to_dataframe()

#From BQ-Prod

def bq_prod(sql) :
#     os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="/home/yogi/credentials/credential_bq_paper-prod.json"
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/leon_2/app/credential_bq_paper-prod.json"

#     gcp_project = 'paper-prod'
#     bq_dataset = 'datascience_public'

#     client = bigquery.Client(gcp_project)
#     dataset_ref = client.dataset(bq_dataset)
#     df = gcp2df(sql, client)
    df = pd.read_gbq(sql, use_bqstorage_api = True)
    return df

def bq_test(sql) :
#     os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="/home/yogi/credentials/skilled-compass-218404-0b257c48b00b.json"
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/leon_2/app/credential_bq_paper-prod.json"

#     gcp_project = 'skilled-compass-218404'
#     bq_dataset = 'datascience_public'

#     client = bigquery.Client(gcp_project)
#     dataset_ref = client.dataset(bq_dataset)
#     df = gcp2df(sql, client)
    df = pd.read_gbq(sql, use_bqstorage_api = True)
    return df

#From Invoicer/Gateway/Payments
def invoicer_gateway(database, sql) :
    """
    invoicer_gateway(database, sql)
    """
    #open database connection
    # host="paper.id"
    host = "34.101.80.240"
    port= 14045
    user="data_team_new"
    password="XmWcStOXkJQV9pGL2RhRx0VmW"
    invoicer="paper_invoicer"
    gateway="paper_gateway"
    payment = "paper_payment"
    db=pymysql.connect(host=host,
                       port=port,
                       user=user,
                       password=password,
                       database=gateway)
    di=pymysql.connect(host=host,
                       port=port,
                       user=user,
                       password=password,
                       database=invoicer)
    dp=pymysql.connect(host=host,
                   port=port,
                   user=user,
                   password=password,
                   database=payment)
    print("connected")
    if database == 'invoicer' :
        query_result = pd.read_sql(sql,di)
    elif database == 'gateway' :
        query_result = pd.read_sql(sql,db)
    elif database == 'payment' :
        query_result = pd.read_sql(sql,dp)
    else :
        query_result = 'error! pick one out of these : invoicer, gateway, payment'
        
    return query_result

#Pull Google Sheets
def pull_gsheets(sheet_name, tab_sheet, index) :
    """
    pull_gsheets(sheet_name, tab_sheet, index)
    """
    client   = pygsheets.authorize(service_file = '/leon_2/app/skilled-compass-218404-0b257c48b00b.json')
#     client   = pygsheets.authorize(service_file = '/home/yogi/leon_2/bd_external/app/skilled-compass-218404-0b257c48b00b.json')
    sheet = client.open(sheet_name)
    worksheet = sheet.worksheet('title', tab_sheet)
    
    df = pd.DataFrame(worksheet.get_all_values(include_tailing_empty_rows = False, include_tailing_empty = False))
    df.columns = df.iloc[index-1]
    df.drop([index-1], inplace = True, axis = 0)
    df.reset_index(inplace = True, drop = True)
    return df

#Push Google Sheets
def push_gsheets(sheet_name, tab_sheet, cell, df, copy_head=True) :
    """
    push_gsheets(sheet_name, tab_sheet, cell, df, copy_head=True)
    """
    client   = pygsheets.authorize(service_file = '/leon_2/app/skilled-compass-218404-0b257c48b00b.json')
#     client   = pygsheets.authorize(service_file = '/home/yogi/leon_2/bd_external/app/skilled-compass-218404-0b257c48b00b.json')
    sheet = client.open(sheet_name)
    worksheet = sheet.worksheet('title', tab_sheet)
    
    worksheet.set_dataframe(df, cell, copy_index=False, copy_head=copy_head, extend=True, fit=False, escape_formulae=False)
    print('Store to Google Sheet Done!')
    
    
#Push to BQ
def store_bq_new(raw, table_name, environment, mode):
    """
    store_bq_new(raw, table_name, environment, mode)
    """
    if environment == 'testing' :
        PROJECTID_PPRPROD = 'skilled-compass-218404'
        dataset           = 'data_science_public'
#         os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/yogi/credentials/skilled-compass-218404-0b257c48b00b.json"
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/leon_2/app/skilled-compass-218404-0b257c48b00b.json"
    elif environment == 'prod' :
        PROJECTID_PPRPROD = 'paper-prod'
        dataset           = 'datascience_public'
#         os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/yogi/credentials/credential_bq_paper-prod.json"
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/leon_2/app/credential_bq_paper-prod.json"
    else :
        return print('error environment! testing or prod only!')

    print(raw.head())
    
    if mode == 'replace' :
        if_exist = mode
    elif mode == 'append' :
        if_exist = mode
    else :
        return print('error mode! append or replace only!')

    # Store to BQ 
    pandas_gbq.to_gbq(raw,'{}.{}'.format(dataset,table_name),
                      project_id=PROJECTID_PPRPROD, 
                      if_exists = if_exist, 
                      location = 'asia-southeast1')
    del raw
    raw = pd.DataFrame()
    print("store to bigquery done {} {}".format(dataset, table_name))