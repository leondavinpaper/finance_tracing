import pandas as pd
import numpy as np
import sys
import template as tp
import pyArango
from pyArango.connection import *

pd.set_option('display.max_columns', None)
import warnings
warnings.filterwarnings('ignore')
import re

conn = Connection(arangoURL='https://arangodb-replication.paper.id/', username="metabase", password="PaperMetabase#2021")
paper_payment = conn['paper_payment']
paper_chain = conn['paper_chain_document_flow']

def get_mst(company_ids) :
    company_id = "'"
    company_id += "', '".join(company_ids)
    company_id += "'"
    
    mst = """select uuid as company_id, company_name, company_email, company_phone from companies
    where uuid in ({})""".format(company_id)
    
    mst = tp.invoicer_gateway('gateway', mst)
    
    company_names = dict(zip(mst.company_id, mst.company_name))
    company_emails = dict(zip(mst.company_id, mst.company_email))
    company_phones = dict(zip(mst.company_id, mst.company_phone))
    
    return company_names, company_emails, company_phones

def get_multi_trx_info(external_ids) :
    df = pd.DataFrame(columns = ['external_id'], data = external_ids)
    
    external_id = '["'
    external_id += '", "'.join(external_ids)
    external_id += '"]'
    
    count_row_trx = """
    for i in digital_payment_transactions
        filter i.external_id in {ext}
        collect with count into length
        return length
    """.format(ext = external_id)

    trx_length = paper_payment.AQLQuery(count_row_trx, batchSize=8)
    trx_length = trx_length.response['result'][0]
    
    if trx_length == 0 :
        print('No Trx Found')
        trx = pd.DataFrame(columns = ['external_id', 'status', 'payment_method', 'payment_detail.payment_channel', 'grand_total', 'company_id', 'payer_email', 'phone', 'name', 'payment_request_id', 'created_at'])
        req = pd.DataFrame(columns = ['company.company_name', 'company.company_email', 'company.company_phone', 'items', 'payment_request_id', 'customer.name', 'customer.email', 'customer.phone'])
        inv = pd.DataFrame(columns = ['document_number', 'partner_data.email', 'partner_data.name', 'partner_data.phone', 'document_info.uuid', '_id', 'doc_type_id'])
        dpr = pd.DataFrame(columns = ['_key', 'supplier_fee', 'buyer_fee'])
        disb = pd.DataFrame(columns = ['disbursement_request_no', 'disbursement_amount'])
    else :
        # Get Trx
        trx_AQL = """for i in digital_payment_transactions
        filter i.external_id in {ext}
        return i""".format(ext = external_id)

        trx = paper_payment.AQLQuery(trx_AQL, batchSize=trx_length)
        trx = trx.response['result']
        
        trx = pd.json_normalize(trx)
        
        # Get PRTs
        payment_request_ids = list(trx['payment_request_id'].unique())
        
        payment_request_id = '["'
        payment_request_id += '", "'.join(payment_request_ids)
        payment_request_id += '"]'
        
        count_row_trx = """
        for i in digital_payment_documents
            filter i.payment_request_id in {ext}
            collect with count into length
            return length
        """.format(ext = payment_request_id)

        trx_length = paper_payment.AQLQuery(count_row_trx, batchSize=8)
        trx_length = trx_length.response['result'][0]

        trx_AQL = """for i in digital_payment_documents
        filter i.payment_request_id in {ext}
        return i""".format(ext = payment_request_id)

        req = paper_payment.AQLQuery(trx_AQL, batchSize=trx_length)
        req = req.response['result']
        
        req = pd.json_normalize(req)
        req['invoice_id'] = req['items'].apply(lambda x: x[0]['invoice_id'])
        
        #DPR
        count_row_trx = """
        for i in digital_payment_requests
            filter i._key in {ext}
            collect with count into length
            return length
        """.format(ext = payment_request_id)

        trx_length = paper_payment.AQLQuery(count_row_trx, batchSize=8)
        trx_length = trx_length.response['result'][0]

        trx_AQL = """for i in digital_payment_requests
        filter i._key in {ext}
        return i""".format(ext = payment_request_id)

        dpr = paper_payment.AQLQuery(trx_AQL, batchSize=trx_length)
        dpr = dpr.response['result']
        
        dpr = pd.json_normalize(dpr)
        dpr = dpr.rename(columns = {'amount.supplier_fee_amount' : 'supplier_fee',
                                    'amount.buyer_fee_amount' : 'buyer_fee'})
        
        #Disbursement Transactions
        count_row_trx = """
        for i in digpay_disbursement_transactions
            filter i.disbursement_request_no in {ext}
            collect with count into length
            return length
        """.format(ext = external_id)

        trx_length = paper_payment.AQLQuery(count_row_trx, batchSize=8)
        trx_length = trx_length.response['result'][0]

        if trx_length > 0 :
            trx_AQL = """for i in digpay_disbursement_transactions
            filter i.disbursement_request_no in {ext}
            return i""".format(ext = external_id)

            disb = paper_payment.AQLQuery(trx_AQL, batchSize=trx_length)
            disb = disb.response['result']

            disb = pd.json_normalize(disb)
            disb = disb[['disbursement_request_no', 'disbursement_amount']]
        else :
            disb = pd.DataFrame(columns = ['disbursement_request_no', 'disbursement_amount'])
            
        #Disbursement Purchase Invoice    
        count_row_trx = """
        for i in purchase_invoice_disbursements
            filter i.external_id in {ext}
            collect with count into length
            return length
        """.format(ext = external_id)

        trx_length = paper_payment.AQLQuery(count_row_trx, batchSize=8)
        trx_length = trx_length.response['result'][0]
            
        if trx_length > 0 :
            trx_AQL = """for i in purchase_invoice_disbursements
            filter i.external_id in {ext}
            return i""".format(ext = external_id)

            disb2 = paper_payment.AQLQuery(trx_AQL, batchSize=trx_length)
            disb2 = disb2.response['result']

            disb2 = pd.json_normalize(disb2)
            disb2 = disb2[['external_id', 'disbursement_amount']]
            disb2.columns = ['disbursement_request_no', 'disbursement_amount']

            disb = disb.append(disb2)
            disb.reset_index(inplace = True, drop = True)
            
        else :
            pass

        #Disbursement PRT
        count_row_trx = """
        for i in payment_reconciliation_transactions
            filter i.external_id in {ext}
            collect with count into length
            return length
        """.format(ext = external_id)

        trx_length = paper_payment.AQLQuery(count_row_trx, batchSize=8)
        trx_length = trx_length.response['result'][0]

        if trx_length > 0 :
            trx_AQL = """for i in payment_reconciliation_transactions
            filter i.external_id in {ext}
            return i""".format(ext = external_id)

            disb3 = paper_payment.AQLQuery(trx_AQL, batchSize=trx_length)
            disb3 = disb3.response['result']

            disb3 = pd.json_normalize(disb3)
            disb3 = disb3[['external_id', 'amount.grand_total']]
            disb3.columns = ['disbursement_request_no', 'disbursement_amount']

            disb = disb.append(disb3)
            disb.reset_index(inplace = True, drop = True)

        else :
            pass
        
        # Get Paperchain Documents
        invoice_ids = list(req['invoice_id'].unique())
        
        invoice_id = '["'
        invoice_id += '", "'.join([x for x in invoice_ids if x != ''])
        invoice_id += '"]'
        
        if invoice_id == '[""]' :
            inv = pd.DataFrame(columns = ['document_number', 'partner_data.email', 'partner_data.name', 'partner_data.phone', 'document_info.uuid', '_id', 'doc_type_id'])
        else :
            count_row_trx = """
            for i in paper_chain_documents
                filter i._id in {ext}
                collect with count into length
                return length
            """.format(ext = invoice_id)

            trx_length = paper_chain.AQLQuery(count_row_trx, batchSize=8)
            trx_length = trx_length.response['result'][0]
            
            if trx_length == 0 :
                inv = pd.DataFrame(columns = ['document_number', 'partner_data.email', 'partner_data.name', 'partner_data.phone', 'document_info.uuid', '_id', 'doc_type_id'])
            else :
                trx_AQL = """for i in paper_chain_documents
                filter i._id in {ext}
                return i""".format(ext = invoice_id)

                inv = paper_chain.AQLQuery(trx_AQL, batchSize=trx_length)
                inv = inv.response['result']

                inv = pd.json_normalize(inv)
        
    return trx, req, inv, dpr, disb

def combine(external_ids, trx, req, inv, dpr, disb) :
    try :
        trx2 = trx[['external_id', 'status', 'payment_method', 'payment_detail.payment_channel', 'grand_total', 'company_id', 'payer_email', 'phone', 'name', 'payment_request_id', 'created_at']]
    except : 
        trx2 = trx[['external_id', 'status', 'payment_method', 'grand_total', 'company_id', 'payer_email', 'phone', 'name', 'payment_request_id', 'created_at']]
        
    req2 = req[['company.company_name', 'company.company_email', 'company.company_phone', 'items', 'payment_request_id',
                'customer.name', 'customer.email', 'customer.phone']]
    
    req2['invoice_id'] = req2['items'].apply(lambda x: x[0]['invoice_id'] if pd.isnull(x) == False else x)

    inv2 = inv[['document_number', 'partner_data.email', 'partner_data.name', 'partner_data.phone', 'document_info.uuid', '_id', 'doc_type_id']]

    dpr2 = dpr[['_key', 'supplier_fee', 'buyer_fee']]
    disb2 = disb[['disbursement_request_no', 'disbursement_amount']]
    
    trx2 = trx2.merge(req2, on = 'payment_request_id', how = 'left')
    trx2 = trx2.merge(inv2, left_on = 'invoice_id', right_on = '_id', how = 'left')
    trx2 = trx2.merge(dpr2, left_on = 'payment_request_id', right_on = '_key', how = 'left')
    trx2 = trx2.merge(disb2, left_on = 'external_id', right_on = 'disbursement_request_no', how = 'left')
    
    df = pd.DataFrame(data = {'external_id' : external_ids})
    
    trx2 = df.merge(trx2, on = 'external_id', how = 'left')
    trx2['invoice_document_type_id'] = trx2['items'].apply(lambda x: x[0]['invoice_document_type_id'] if pd.isnull(x) == False else x)
    trx2.loc[pd.isnull(trx2['doc_type_id']), 'doc_type_id'] = trx2['invoice_document_type_id']
    
    trx2['trx_type'] = 'No Trx'
    trx2.loc[((pd.isnull(trx2['doc_type_id'])) | (trx2['doc_type_id'] == '')) & (pd.isnull(trx2['payment_request_id']) == False), 'trx_type'] = 'PYOR'
    trx2.loc[(trx2['doc_type_id'] == 'inv-11') & (pd.isnull(trx2['payment_request_id']) == False), 'trx_type'] = 'In'
    trx2.loc[(trx2['doc_type_id'] == 'inv-12') & (pd.isnull(trx2['payment_request_id']) == False), 'trx_type'] = 'Out'
    
    trx2['invoice_document_number'] = trx2['items'].apply(lambda x: x[0]['invoice_number'] if pd.isnull(x) == False else x)
    trx2.loc[pd.isnull(trx2['document_number']), 'document_number'] = trx2['invoice_document_number']
    
    try :
        trx3 = trx2[['external_id', 'trx_type', 'status', 'document_number', 'document_info.uuid', 'payment_method', 
                     'payment_detail.payment_channel', 'grand_total', 'company_id', 'company.company_name', 
                     'company.company_email', 'company.company_phone', 'customer.name', 'customer.email', 'customer.phone', 'payer_email',
                     'phone','name', 'created_at', 'buyer_fee', 'supplier_fee', 'disbursement_amount']]
    except :
        trx3 = trx2[['external_id', 'trx_type', 'status', 'document_number', 'document_info.uuid', 'payment_method',
                     'grand_total', 'company_id', 'company.company_name', 'company.company_email', 'company.company_phone',
                     'customer.name', 'customer.email', 'customer.phone', 'payer_email', 'phone','name', 'created_at',
                     'buyer_fee', 'supplier_fee', 'disbursement_amount']]
    
    return trx2, trx3

def get_kyc(df) :
    company_id = "'"
    company_id += "', '".join([x for x in df['company_id'].unique() if pd.isnull(x) == False])
    company_id += "'"
    
    kyc = tp.invoicer_gateway('gateway', """select * from kyc_basics where company_id in ({})""".format(company_id))
    df = df.merge(kyc[['company_id', 'status']], on = 'company_id', how = 'left')
    
    return kyc, df

def get_va_out(external_ids) :
    external_id = '["'
    external_id += '", "'.join(external_ids)
    external_id += '"]'
    
    count_row_trx = """
    for i in purchase_invoice_disbursements
        filter i.external_id in {ext}
        collect with count into length
        return length
    """.format(ext = external_id)

    trx_length = paper_payment.AQLQuery(count_row_trx, batchSize=8)
    trx_length = trx_length.response['result'][0]

    trx_AQL = """for i in purchase_invoice_disbursements
    filter i.external_id in {ext}
    return i""".format(ext = external_id)

    va = paper_payment.AQLQuery(trx_AQL, batchSize=trx_length)
    va = va.response['result']
    
    va = pd.json_normalize(va)
    va = va[['external_id', 'finance_account.bank_account_number', 'finance_account.bank_name', 'partner_name']]
    
    va = va.rename(columns = {'finance_account.bank_account_number' : 'bank_account_no',
                              'finance_account.bank_name' : 'bank_name'})
    
    va_out_bank_account_no = dict(zip(va.external_id, va.bank_account_no))
    va_out_bank_name = dict(zip(va.external_id, va.bank_name))
    pyor_partner_name = dict(zip(va.external_id, va.partner_name))
    
    return va, va_out_bank_account_no, va_out_bank_name

def get_va_in(company_ids) :
    company_id = "'"
    company_id += "' ,'".join(company_ids)
    company_id += "'"
    
    sql = """
    select settlement_finance_accounts.company_id, finance_accounts.bank_account_no, 
    finance_accounts.bank_name 
    from settlement_finance_accounts 
    inner join finance_accounts 
    on settlement_finance_accounts.finance_account_id = finance_accounts.uuid
    
    where settlement_finance_accounts.company_id in ({})
    """.format(company_id)
    
    va = tp.invoicer_gateway('invoicer', sql)
    
    va_in_bank_account_no = dict(zip(va.company_id, va.bank_account_no))
    va_in_bank_name = dict(zip(va.company_id, va.bank_name))
    
    return va, va_in_bank_account_no, va_in_bank_name

def get_pyor_partner(df) :
    company_ids = list(df['company_id'].unique())
    
    company_id = "'"
    company_id += "', '".join(company_ids)
    company_id += "'"
    
    partners = """select company_id, uuid as partner_id, name as partner_name, email as partner_email, phone as partner_phone
    from partners
    where company_id in ({})""".format(company_id)
    
    partners = tp.invoicer_gateway('invoicer', partners)
    
    df = df.merge(partners, left_on = ['company_id', 'company.company_name'], right_on = ['company_id', 'partner_name'], how = 'left')
    pyor_partner_name = dict(zip(df.external_id, df.partner_name))
    pyor_partner_email = dict(zip(df.external_id, df.partner_email))
    pyor_partner_phone = dict(zip(df.external_id, df.partner_phone))
    
    return partners, pyor_partner_name, pyor_partner_email, pyor_partner_phone

def get_subs_package(df) :
    company_ids = list(df['company_id'].unique())
    
    company_id = "'"
    company_id += "', '".join(company_ids)
    company_id += "'"
    
    subs = """select subscriptions.company_id, subscriptions.package_id, packages.package_name as package_active, subscriptions.created_at
    from subscriptions
    left join packages
    on subscriptions.package_id = packages.uuid
    where company_id in ({})""".format(company_id)
    
    subs = tp.invoicer_gateway('gateway', subs)
    subs = subs.sort_values(by = 'created_at', ascending = False)
    subs = subs.drop_duplicates(subset = ['company_id'], keep = 'first')
    subs.reset_index(inplace = True, drop = True)
    
    df = df.merge(subs[['company_id', 'package_active']], on = 'company_id', how = 'left')
    
    return subs, df

def main(external_ids) :
    trx, req, inv, dpr, disb = get_multi_trx_info(external_ids)
    print('Step 1 Done!')
    trx2, trx3 = combine(external_ids, trx, req, inv, dpr, disb)
    print('Step 2 Done!')
    kyc, trx3 = get_kyc(trx3)
    print('Done Getting KYC!')
    subs, trx3 = get_subs_package(trx3)
    print('Done Getting Package!')
    
    list_outs = list(trx3[trx3['trx_type'].isin(['PYOR', 'Out'])]['external_id'].unique())
    list_ins = list(trx3[trx3['trx_type'].isin(['In'])]['company_id'].unique())
    
    trx3['bank_account_no'] = np.nan
    trx3['bank_name'] = np.nan
    
    if len(list_ins) > 0 :
        va_in, va_in_bank_account_no, va_in_bank_name = get_va_in(list(trx3[trx3['trx_type'].isin(['In'])]['company_id'].unique()))
        trx3['bank_account_no'] = trx3['company_id'].map(va_in_bank_account_no)
        trx3['bank_name'] = trx3['company_id'].map(va_in_bank_name)
        
    if len(list_outs) > 0 :
        va_out, va_out_bank_account_no, va_out_bank_name = get_va_out(list(trx3[trx3['trx_type'].isin(['PYOR', 'Out'])]['external_id'].unique()))
        trx3['bank_account_no'] = trx3['external_id'].map(va_out_bank_account_no).fillna(trx3['bank_account_no'])
        trx3['bank_name'] = trx3['external_id'].map(va_out_bank_name).fillna(trx3['bank_name'])
    
    
    print('Done Getting VA!')
    
    trx3['company_name'] = trx3['company.company_name']
    trx3['company_email'] = trx3['company.company_email']
    trx3['company_phone'] = trx3['company.company_phone']
    trx3['partner_name'] = trx3['customer.name']
    trx3['partner_email'] = trx3['customer.email']
    trx3['partner_phone'] = trx3['customer.phone']

    trx3.loc[trx3['trx_type'].isin(['PYOR', 'Out']), 'company_name'] = trx3['customer.name']
    trx3.loc[trx3['trx_type'].isin(['PYOR', 'Out']), 'company_email'] = trx3['customer.email']
    trx3.loc[trx3['trx_type'].isin(['PYOR', 'Out']), 'company_phone'] = trx3['customer.phone']
    trx3.loc[trx3['trx_type'].isin(['PYOR', 'Out']), 'partner_name'] = trx3['company.company_name']
    trx3.loc[trx3['trx_type'].isin(['PYOR', 'Out']), 'partner_email'] = trx3['company.company_email']
    trx3.loc[trx3['trx_type'].isin(['PYOR', 'Out']), 'partner_phone'] = trx3['company.company_phone']
    
    try :
        trx3 = trx3[['external_id', 'trx_type', 'status_x', 'document_number',
                   'document_info.uuid', 'payment_method', 'payment_detail.payment_channel', 'grand_total',
                   'company_id', 'status_y', 'package_active',
                   'bank_account_no', 'bank_name', 'company_name', 'company_email',
                   'company_phone', 'partner_name', 'partner_email', 'partner_phone', 'created_at',
                    'buyer_fee', 'supplier_fee', 'disbursement_amount']]

        trx3 = trx3.rename(columns = {'trx_type' : 'payment_type',
                                      'status_x' : 'transaction_status',
                                      'document_info.uuid' : 'document_uuid',
                                      'payment_detail.payment_channel' : 'payment_channel',
                                      'grand_total' : 'trx_amount',
                                      'status_y' : 'status_kyc',
                                      'bank_account_no' : 'disb_bank_number',
                                      'bank_name' : 'disb_bank_account',
                                      'created_at' : 'payment_date'})

        trx3 = trx3[['external_id', 'payment_type', 'transaction_status', 'document_number', 
                     'payment_method', 'payment_channel', 'trx_amount', 'company_id', 'company_name',
                     'company_email', 'status_kyc', 'package_active', 'disb_bank_account', 
                     'disb_bank_number', 'partner_name', 'partner_email','partner_phone', 'payment_date',
                     'buyer_fee', 'supplier_fee', 'disbursement_amount']]
    except :
        trx3 = trx3[['external_id', 'trx_type', 'status_x', 'document_number',
                   'document_info.uuid', 'payment_method', 'grand_total',
                   'company_id', 'status_y', 'package_active',
                   'bank_account_no', 'bank_name', 'company_name', 'company_email',
                   'company_phone', 'partner_name', 'partner_email', 'partner_phone', 'created_at',
                    'buyer_fee', 'supplier_fee', 'disbursement_amount']]

        trx3 = trx3.rename(columns = {'trx_type' : 'payment_type',
                                      'status_x' : 'transaction_status',
                                      'document_info.uuid' : 'document_uuid',
                                      'grand_total' : 'trx_amount',
                                      'status_y' : 'status_kyc',
                                      'bank_account_no' : 'disb_bank_number',
                                      'bank_name' : 'disb_bank_account',
                                      'created_at' : 'payment_date'})

        trx3 = trx3[['external_id', 'payment_type', 'transaction_status', 'document_number', 
                     'payment_method', 'trx_amount', 'company_id', 'company_name',
                     'company_email', 'status_kyc', 'package_active', 'disb_bank_account', 
                     'disb_bank_number', 'partner_name', 'partner_email','partner_phone', 'payment_date',
                     'buyer_fee', 'supplier_fee', 'disbursement_amount']]
    
    trx3.loc[trx3['payment_type'] == 'PYOR', 'payment_type'] = 'Out'
    trx3.loc[pd.isnull(trx3['status_kyc']), 'status_kyc'] = ''
    trx3.loc[pd.isnull(trx3['document_number']), 'document_number'] = ''
    
    company_ids = [x for x in trx3['company_id'].unique() if pd.isnull(x) == False]
    company_names, company_emails, company_phones = get_mst(company_ids)
    
    trx3['company_name'] = trx3['company_id'].map(company_names).fillna(trx3['company_name'])
    trx3['company_email'] = trx3['company_id'].map(company_emails).fillna(trx3['company_email'])
#     trx3['company_phone'] = trx3['company_id'].map(company_phones).fillna(trx3['company_phone'])
    
    trx3['payment_date'] = trx3['payment_date'].apply(lambda x: str(x).split('T')[0])
    
    trx3 = trx3.drop_duplicates(keep = 'first')
    trx3.reset_index(inplace = True, drop = True)
    
    print(trx3.columns)
    print(trx3.info())
    
    return trx3